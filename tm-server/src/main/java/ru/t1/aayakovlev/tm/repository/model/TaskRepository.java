package ru.t1.aayakovlev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface TaskRepository extends ExtendedRepository<Task> {

    @NotNull
    Task create(
            @NotNull final String userId,
            @NotNull final String name
    );

    @NotNull
    Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );


}
