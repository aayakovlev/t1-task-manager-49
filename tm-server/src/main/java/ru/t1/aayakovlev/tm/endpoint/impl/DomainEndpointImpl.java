package ru.t1.aayakovlev.tm.endpoint.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.endpoint.DomainEndpoint;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AccessDeniedException;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.service.AuthService;
import ru.t1.aayakovlev.tm.service.DomainService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.aayakovlev.tm.endpoint.DomainEndpoint")
public final class DomainEndpointImpl extends AbstractEndpoint implements DomainEndpoint {

    public DomainEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private DomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @NotNull
    private AuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    private PropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse backupLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().backupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse backupSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().backupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse base64Load(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().base64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse base64Save(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().base64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse binaryLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().binaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse binarySave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().binarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXmlResponse jsonLoadFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().jsonLoadFXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxBResponse jsonLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().jsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXmlResponse jsonSaveFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().jsonSaveFXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxBResponse jsonSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().jsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadFasterXmlResponse xmlLoadFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().xmlLoadFXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadJaxBResponse xmlLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().xmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveFasterXmlResponse xmlSaveFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().xmlSaveFXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveJaxBResponse xmlSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().xmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXmlResponse yamlLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().yamlLoad();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXmlResponse yamlSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().yamlSave();
        return new DataYamlSaveFasterXmlResponse();
    }

    @WebMethod
    @SneakyThrows
    public void initScheme(
            @WebParam(name = "initToken") @Nullable final String initToken
    ) {
        @NotNull final String token = getPropertyService().getDatabaseInitToken();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        getDomainService().initScheme();
    }

    @WebMethod
    @SneakyThrows
    public void dropScheme(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        getDomainService().dropScheme();
    }

}
