package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.AbstractExtendedModelDTO;

import java.util.Comparator;
import java.util.List;

public interface ExtendedDTORepository<E extends AbstractExtendedModelDTO> extends BaseDTORepository<E> {

    int count(@NotNull final String userId);

    void clear(@NotNull final String userId);

    @NotNull
    List<E> findAll(@NotNull final String userId);

    @NotNull
    List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator);

    @Nullable
    E findById(@NotNull final String userId, @NotNull final String id);

    void removeById(@NotNull final String userId, @NotNull final String id);

}
