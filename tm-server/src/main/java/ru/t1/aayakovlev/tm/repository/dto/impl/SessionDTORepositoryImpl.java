package ru.t1.aayakovlev.tm.repository.dto.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class SessionDTORepositoryImpl extends AbstractExtendedDTORepository<SessionDTO>
        implements SessionDTORepository {

    public SessionDTORepositoryImpl(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";
    }

}
