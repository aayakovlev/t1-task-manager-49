package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.model.SessionRepository;
import ru.t1.aayakovlev.tm.repository.model.impl.SessionRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.model.SessionService;

import javax.persistence.EntityManager;

public final class SessionServiceImpl extends AbstractExtendedService<Session, SessionRepository> implements SessionService {

    public SessionServiceImpl(@NotNull final ConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected SessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepositoryImpl(entityManager);
    }

}
