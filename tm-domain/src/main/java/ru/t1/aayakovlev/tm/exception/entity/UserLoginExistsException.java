package ru.t1.aayakovlev.tm.exception.entity;

public final class UserLoginExistsException extends AbstractEntityException {

    public UserLoginExistsException() {
        super("Error! User with entered login already exists...");
    }

}
