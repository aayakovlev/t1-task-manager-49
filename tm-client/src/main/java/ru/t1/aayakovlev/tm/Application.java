package ru.t1.aayakovlev.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.component.Bootstrap;

public final class Application {

    public static void main(@NotNull String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.run(args);
        } catch (@NotNull final Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
