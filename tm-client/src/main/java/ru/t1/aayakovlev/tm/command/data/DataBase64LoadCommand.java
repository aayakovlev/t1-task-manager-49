package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.DataBase64LoadRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-base64-load";

    @NotNull
    private static final String DESCRIPTION = "Load data from base64 file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD BASE64]");
        getDomainEndpoint().base64Load(new DataBase64LoadRequest(getToken()));
    }

}
