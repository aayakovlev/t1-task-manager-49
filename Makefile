clean:
	mvn clean

install:
	mvn install

install-no-tests:
	mvn install -DskipTests

clean-install:
	mvn clean install

clean-install-no-tests:
	mvn clean install -DskipTests

db-up:
	docker run -d \
    	--rm \
    	--name tm-db \
    	-e POSTGRES_PASSWORD=postgres \
    	-e POSTGRES_DB=task_manager \
    	-p 35432:5432 \
    	postgres:14-alpine

db-down:
	docker stop tm-db
