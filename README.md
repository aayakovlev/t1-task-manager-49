# TASK MANAGER

## DEVELOPER

**NAME**: Yakovlev Anton

**E-MAIL**: aayakovlev@t1-consulting.ru

## SOFTWARE

**OS**: Windows 10 LTSC 1809 (build 17763.2628)

**JDK**: OpenJDK 1.8.0_41

**MAVEN**: 3.6.3

## HARDWARE

**CPU**: AMD R9

**RAM**: 32 Gb

**SSD**: NVMe 512 Gb

## APPLICATION PROPERTIES

| OS ENV             | JAVA OPTS            | DEFAULT VALUE                                 | MEANING                                 | CLIENT | SERVER |
|--------------------|----------------------|-----------------------------------------------|-----------------------------------------|--------|--------|
| CONFIG             | -Dconfig             | ./application.properties                      | external application configuration file | ✔      | ✔      |
| PASSWORD_ITERATION | -Dpassword.iteration | 4321                                          | password hash iterations                |        | ✔      |
| PASSWORD_SECRET    | -Dpassword.secret    | 12321                                         | password hash salt                      |        | ✔      |
| BACKUP_ENABLED     | -Dbackup.enabled     | false                                         | disable/enable backup                   |        | ✔      |
| SERVER_PORT        | -Dserver.port        | 10340                                         | server port                             | ✔      | ✔      |
| SERVER_HOST        | -Dserver.host        | 127.0.0.1                                     | server host                             | ✔      | ✔      |
| SESSION_KEY        | -Dsession.key        | 123654321                                     | key for encrypt/decrypt session         |        | ✔      |
| SESSION_TIMEOUT    | -Dsession.timeout    | 10800                                         | timeout of session in seconds           |        | ✔      |
| DATABASE_URL       | -Ddatabase.url       | jdbc:postgresql://127.0.0.1:5432/task_manager | database connection url                 |        | ✔      |
| DATABASE_USERNAME  | -Ddatabase.username  | postgres                                      | database user                           |        | ✔      |
| DATABASE_PASSWORD  | -Ddatabase.password  | postgres                                      | database password                       |        | ✔      |

## BUILD APPLICATION

````shell
mvn clean install
````

## RUN APPLICATION

### SERVER
````shell
java -jar ./task-manager-server.jar
````

### CLIENT
````shell
java -jar ./task-manager-client.jar
````
